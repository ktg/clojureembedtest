import java.io.IOException;
import java.io.StringReader;

import clojure.lang.Compiler;
import clojure.lang.RT;
import clojure.lang.Var;

public class Main {
    public static void main(String[] args) throws IOException, ClassNotFoundException {        
        //clojure.main.main(args);
        
        // Load the Clojure script -- as a side effect this initializes the runtime.
        String str = "(ns user) (defn foo [a b]   (str a \" \" b)) (def a 3)";

        //RT.loadResourceScript("foo.clj");
        Compiler.load(new StringReader(str));

        // Get a reference to the foo function.
        Var foo = RT.var("user", "foo");
        Var a = RT.var("user", "a"); // reference to the variable a

        // Call it!
        Object result = foo.invoke("Hi", "there");
        System.out.println(result);
        
        System.out.println(a);
        System.out.println(a.get());
    }
}
